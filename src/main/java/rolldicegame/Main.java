package rolldicegame;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Casino casino = new Casino();
        Player player = new Player();
        casino.join(player);

        System.out.println("Welcome to game!");

        Scanner scanner = new Scanner(System.in);

        System.out.println("How many chips would you like to buy?");
        String line = scanner.nextLine();
        player.buyChips(Integer.parseInt(line));

        while (true) {
            System.out.println("How many chips would you like to bet?");
            line = scanner.nextLine();
            Integer chips = Integer.parseInt(line);

            System.out.println("What number would you use for bet?");
            line = scanner.nextLine();
            Integer number = Integer.parseInt(line);

            casino.bet(player, number, chips);

            if (casino.play(player).getResult())
                System.out.println("You win!!!!");
            else
                System.out.println("You lose....");

            System.out.println("You have " + player.getChips() + "chips.");

        }
    }
}
