package rolldicegame;

public class Player {

    private int chips;

    public void buyChips(int countChips) {
        chips += countChips;
    }

    public int getChips() {
        return chips;
    }

    public void addChips(int chips) {
        this.chips += chips;
    }

    public void holdChips(int chips) {
        this.chips -= chips;
    }
}
