package rolldicegame;

import java.util.*;

public class Casino {
    private List<Player> players = new ArrayList<>();
    private Map<Player, Bet> bets = new HashMap<>();

    public void join(Player player) {
        players.add(player);
    }

    public boolean hasPlayer(Player player) {
        return players.contains(player);
    }

    public void bet(Player player, int number, int chips) {
        this.bets.put(player, new Bet(chips, number));
        player.holdChips(chips);
    }

    public GameResult play(Player player) {
        int cubeValue = getNumber();
        Bet bet = bets.get(player);
        GameResult gameResult = new GameResult(false, player.getChips());
        if (bet.getNumber() == cubeValue) {
            player.addChips(bet.getChips() * 6);
            gameResult.setResult(true);
        }
        bets.remove(player);
        gameResult.setChips(player.getChips());

        return gameResult;
    }

    public boolean hasBetForPlayer(Player player) {
        return this.bets.containsKey(player);
    }

    public int getNumber() {
        int minimum = 1, maximum = 6;
        return minimum + (int) (Math.random() * maximum);
    }
}
