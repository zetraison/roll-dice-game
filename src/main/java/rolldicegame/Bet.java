package rolldicegame;

public class Bet {
    private int chips;
    private int number;

    public Bet(int chips, int number) {
        this.chips = chips;
        this.number = number;
    }

    public int getChips() {
        return chips;
    }

    public int getNumber() {
        return number;
    }
}
