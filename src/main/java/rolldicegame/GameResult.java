package rolldicegame;

public class GameResult {
    private boolean result;
    private int chips;

    public GameResult(boolean result, int chips) {
        this.result = result;
        this.chips = chips;
    }

    public boolean getResult() {
        return result;
    }

    public int getChips() {
        return chips;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public void setChips(int chips) {
        this.chips = chips;
    }
}
