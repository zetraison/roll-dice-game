package rolldicegame;

import org.junit.Test;

import static org.junit.Assert.*;

public class CasinoTest {

    private class MyCasino extends Casino {
        public int getNumber() {
            return 1;
        }
    }

    @Test
    public void testPlayerJoinToGame() {
        Casino casino = new Casino();
        Player player = new Player();

        casino.join(player);

        assertTrue(casino.hasPlayer(player));
    }

    @Test
    public void testPlayerBuyChips() {
        Player player = new Player();

        player.buyChips(1);

        assertEquals(player.getChips(), 1);
    }

    @Test
    public void testPlayerBet_betAddedToCasino() {
        Casino casino = new Casino();
        Player player = new Player();
        player.buyChips(1);
        casino.join(player);

        casino.bet(player, 1, 1);

        assertTrue(casino.hasBetForPlayer(player));
        assertEquals(player.getChips(), 0);
    }

    @Test
    public void testCasino_generateRandomNumberWithStab() {
        Casino casino = new MyCasino();

        assertEquals(casino.getNumber(), 1);
    }

    @Test
    public void testCasino_generateRandomNumber() {
        Casino casino = new Casino();

        assertTrue(casino.getNumber() >= 1);
        assertTrue(casino.getNumber() <= 6);
    }

    @Test
    public void testCasino_whenPlayerWinGame() {
        Casino casino = new MyCasino();
        Player player = new Player();
        player.buyChips(1);
        casino.join(player);

        casino.bet(player, 1, 1);
        GameResult result = casino.play(player);

        assertTrue(result.getResult());
        assertEquals(player.getChips(), 6);
        assertEquals(casino.hasBetForPlayer(player), false);
    }

    @Test
    public void testCasino_whenPlayerLoseGame() {
        Casino casino = new MyCasino();
        Player player = new Player();
        player.buyChips(1);
        casino.join(player);

        casino.bet(player, 2, 1);
        GameResult result = casino.play(player);

        assertFalse(result.getResult());
        assertEquals(player.getChips(), 0);
        assertEquals(casino.hasBetForPlayer(player), false);
    }
}
